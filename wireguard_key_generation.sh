#!/usr/bin/env bash

### activa el script dándole permisos de ejecución con:
### chmod u+x wireguard_key_generation.sh
### sustituye los siguientes paámetros:

IP_SERVIDOR_1=xx.xx.xx.xx
IP_SERVIDOR_2=xx.xx.xx.xx
CLAVE_PUB_SERVIDOR_1=clave_publica
CLAVE_PUB_SERVIDOR_2=clave_publica
IP_USUARIO_PC=ip_rango_wireguard
IP_USUARIO_ANDROID=ip_rango_wireguard
DNS=xx.xx.xx.xx

###no cambiar nada a partir de aquí

if [[ "$EUID" -ne 0 ]]; then
    echo "[-] tienes que correr el script como superusuario. sudo ./wireguard_key_generation.sh"
    exit
fi

red=$'\e[1;31m'
grn=$'\e[1;32m'
blu=$'\e[1;34m'
mag=$'\e[1;35m'
cyn=$'\e[1;36m'
white=$'\e[0m'

delimiter="===================================="

cat << "EOF"

\ \        / /_   _|  __ \|  ____/ ____| |  | |  /\   |  __ \|  __ \
 \ \  /\  / /  | | | |__) | |__ | |  __| |  | | /  \  | |__) | |  | |
  \ \/  \/ /   | | |  _  /|  __|| | |_ | |  | |/ /\ \ |  _  /| |  | |
   \  /\  /   _| |_| | \ \| |___| |__| | |__| / ____ \| | \ \| |__| |
    \/  \/   |_____|_|  \_\______\_____|\____/_/    \_\_|  \_\_____/

EOF

sleep 5

echo $delimiter

echo $grn "Arrancando script..." $white

echo $delimiter

echo $grn "Creando directorios..." $white

dir=/etc/wireguard

if [[ ! -e $dir ]]; then
    mkdir wireguard
elif [[ ! -d $dir ]]; then
    echo "$dir el directorio ya existe y no se crea" 1>&2
fi

cd $dir

umask 077

privatekey_pc=privatekey_pc
privatekey_android=privatekey_pc

echo $delimiter

echo $grn "Generando claves..." $white
if [ -e $privatekey_pc ]; then
  echo "Las claves ya existen!"
else
wg genkey | tee privatekey_pc | wg pubkey > publickey_pc
wg genkey | tee privatekey_android | wg pubkey > publickey_android
fi


pc_pub=$( cat publickey_pc )
android_pub=$( cat publickey_android )

pc_priv=$( cat privatekey_pc )
android_priv=$( cat privatekey_android )

echo $delimiter

echo $grn "Creando fichero de configuración Wireguard..." $white

wg1=wg0_pc_suiza.conf

if [ -e $wg1 ]; then
  echo "el fichero para PC Suiza ya existe!"
else
  cat >> wg0_pc_suiza.conf <<EOF

  [Interface]
  Address = $IP_USUARIO_PC/32
  DNS = DNS
  PrivateKey = $pc_priv

  [Peer]
  PublicKey = $CLAVE_PUB_SERVIDOR_1
  AllowedIPs = 0.0.0.0/0
  Endpoint = $IP_SERVIDOR_1:51820
  PersistentKeepalive = 21

EOF

fi

wg2=wg0_android_suiza.conf

if [ -e $wg2 ]; then
  echo "el fichero para android Suiza ya existe!"
else
  cat >> wg0_android_suiza.conf <<EOF

  [Interface]
  Address = $IP_USUARIO_ANDROID/32
  DNS = DNS
  PrivateKey = $android_priv

  [Peer]
  PublicKey = $CLAVE_PUB_SERVIDOR_1
  AllowedIPs = 0.0.0.0/0
  Endpoint = $IP_SERVIDOR_1:51820
  PersistentKeepalive = 21

EOF

fi

wg3=wg0_pc_alemania.conf

if [ -e $wg3 ]; then
  echo "el fichero para PC Alemania ya existe!"
else
  cat >> wg0_pc_alemania.conf <<EOF

  [Interface]
  Address = $IP_USUARIO_PC/32
  DNS = DNS
  PrivateKey = $pc_priv

  [Peer]
  PublicKey = $CLAVE_PUB_SERVIDOR_2
  AllowedIPs = 0.0.0.0/0
  Endpoint = $IP_SERVIDOR_2:51820
  PersistentKeepalive = 21

EOF

fi

wg4=wg0_android_alemania.conf

if [ -e $wg4 ]; then
  echo "el fichero para android Alemania ya existe!"
else
    cat >> wg0_android_alemania.conf <<EOF

  [Interface]
  Address = $IP_USUARIO_ANDROID/32
  DNS = DNS
  PrivateKey = $android_priv

  [Peer]
  PublicKey = $CLAVE_PUB_SERVIDOR_2
  AllowedIPs = 0.0.0.0/0
  Endpoint = $IP_SERVIDOR_2:51820
  PersistentKeepalive = 21

EOF

fi

echo $delimiter

echo $grn"TUS CLAVES PUBLICAS Y PRIVADAS Y LOS ARCHIVOS DE CONFIGURACIÓN
SE HAN GAURDADO EN EL DIRECTORIO wireguard"$white

echo $delimiter

echo $red"PARA QUE PODAMOS CONFIGURAR EL SERVIDOR, MÁNDANOS
TUS CLAVES PÚBLICAS A: xxx@xxx"$white

echo $delimiter

echo $grn "Mostrando claves públicas..." $white

echo $grn "Clave pública PC" $white:
cat publickey_pc

echo $delimiter

echo $grn "Clave pública Móvil" $white:
cat publickey_android


echo $delimiter

echo $grn"INSTRUCCIONES ANDROID"$white

echo "1. descargate la app Wireguard desde F-droid o desde la app store"$white
echo "2. genera el QR para Suiza con el comando:" $grn"qrencode -t ansiutf8 < wireguard/wg0_android_suiza.conf"$white
echo "3. genera el QR para Alemania con el comando:" $grn"qrencode -t ansiutf8 < wireguard/wg0_android_alemania.conf"$white

echo $grn"INSTRUCCIONES PC"$white

echo "Para facilitarte la gestión de las conexiones recomendamos usar una aplicación gráfica
Para GNU/Linux: https://github.com/corrad1nho/qomui
Para MAC: https://apps.apple.com/us/app/wireguard/id1451685025"

echo "También puedes usar Wireguard desde la terminal:"

echo "1. para conectarte a Alemania usa el comando:" $grn"wg-quick up wireguard/wg0_pc_alemania.conf"$white
echo "1. para desconectarte a Alemania usa el comando:" $grn"wg-quick down wireguard/wg0_pc_alemania.conf"$white
echo "2. para conectarte a Suiza usa el comando:" $grn"wg-quick up wireguard/wg0_pc_suiza.conf"$white
echo "2. para desconectarte a Suiza usa el comando:" $grn"wg-quick wireguard/down wg0_pc_suiza.conf"$white
